using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController :MonoBehaviour {

    private Rigidbody2D _rigidbody2D;
    private SurfaceEffector2D _surfaceEffector2D;

    private float horizontalDirection = 0;

    [SerializeField] float torqueForce;

    private bool isDead = false;

    private void Awake() {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _surfaceEffector2D = FindObjectOfType<SurfaceEffector2D>();
    }

    void Update() {
        // Get Horizontal Value
        horizontalDirection = Input.GetAxis("Horizontal");

        if (!isDead) {
            // Boost
            if (Input.GetKey(KeyCode.Space)) {
                _surfaceEffector2D.speed = 30;
            } else {
                _surfaceEffector2D.speed = 20;
            }
        }
    }

    private void FixedUpdate() {
        if (!isDead) {
            _rigidbody2D.AddTorque(torqueForce * horizontalDirection * -1);
        }
    }

    public void DisableControls() {
        isDead = true;
    }
}
