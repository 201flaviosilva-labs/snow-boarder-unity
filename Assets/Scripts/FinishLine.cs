using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishLine :MonoBehaviour {

    private float restartTime = 0.5f;
    [SerializeField] ParticleSystem _particleSystem;


    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player") {
            _particleSystem.Play();
            GetComponent<AudioSource>().Play();
            Invoke("RestartGame", restartTime);
        }
    }

    void RestartGame() {
        SceneManager.LoadScene(0);
    }
}
