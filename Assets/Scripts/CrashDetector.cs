using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CrashDetector :MonoBehaviour {

    private float restartTime = 0.5f;
    [SerializeField] ParticleSystem _particleSystem;
    [SerializeField] AudioClip crashSFX;

    private PlayerController _playerController;

    private bool isAlreadyCrashed = false;

    private void Awake() {
        _playerController = FindObjectOfType<PlayerController>();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Ground" && !isAlreadyCrashed) {
            isAlreadyCrashed = true;
            _playerController.DisableControls();
            _particleSystem.Play();
            GetComponent<AudioSource>().PlayOneShot(crashSFX);
            Invoke("RestartGame", restartTime);
        }
    }


    void RestartGame() {
        SceneManager.LoadScene(0);
    }
}
